$(document).ready(function() {
  $("#productType").on('change', function() {
      var value = $(this).val();
      $("div.container-fluid").hide();
      $("#show" + value).show();
  });
});

$(document).ready(function() {
  var inputBox = document.getElementById("price");
  var inputBox1 = document.getElementById("size");
  var inputBox2 = document.getElementById("weight");
  var inputBox3 = document.getElementById("price");
  var inputBox4 = document.getElementById("height");
  var inputBox5 = document.getElementById("width");
  var inputBox6 = document.getElementById("length");

  const inputs=[inputBox,inputBox1,inputBox2,inputBox3,inputBox4,inputBox5, inputBox6];

  var invalidChars = [
      "-",
      "+",
      "e",
  ];

  for(i of inputs)
  {
    i.addEventListener("keydown", function(e) {
        if (invalidChars.includes(e.key)) {
            e.preventDefault();
        }
    });
  }
});


function validate() {
  var sku = document.getElementById("sku").value;
  var name = document.getElementById("name").value;
  let price = document.getElementById("price").value;
  let size = document.getElementById("size").value;
  let weight = document.getElementById("weight").value;
  let height = document.getElementById("height").value;
  let width = document.getElementById("width").value
  let length = document.getElementById("length").value;
  let productType = document.getElementById("productType").value;
  let esku = "";
  let ename = "";
  let eprice = "";
  let esize = "";
  let eheight = "";
  let ewidth = "";
  let elength = "";
  let eselect = "";
  var skuVal = false;
  var nameVal = false;
  var priceVal = false;
  var productTypeVal = false;
  var siezeVal = false;
  var weightVal = false;
  var heightVal = false;
  var widthVal = false;
  var lengthVal = false;
  var furnitureVal = false;

  if (sku.length <= 0) {
      esku = "*SKU is empty*";
      document.getElementById("esku").innerHTML = esku;
      skuVal = false;
  } else {
      esku = "";
      document.getElementById("esku").innerHTML = esku;
      skuVal = true;
  }

  if (name.length <= 0) {
      ename = "*Name is empty*";
      document.getElementById("ename").innerHTML = ename;
      nameVal = false;
  } else {
      esku = "";
      document.getElementById("ename").innerHTML = ename;
      nameVal = true;
  }

  if (price <= 0) {
      eprice = "*Price not valid*";
      document.getElementById("eprice").innerHTML = eprice;
      priceVal = false;
  } else {
      eprice = "";
      document.getElementById("eprice").innerHTML = eprice;
      priceVal = true;
  }

  if (productType == "Type switcher") {
      eselect = "*Product not selected*";
      document.getElementById("eselect").innerHTML = eselect;
      productTypeVal = false;
  } else {
      eselect = "";
      document.getElementById("eselect").innerHTML = eselect;
      productTypeVal = true;
  }

  if (size <= 0) {
      esize = "*Size not valid*";
      document.getElementById("esize").innerHTML = esize;
      siezeVal = false;
  } else {
      esize = "";
      document.getElementById("esize").innerHTML = esize;
      siezeVal = true;
  }

  if (weight <= 0) {
      eweight = "*Weight not valid*";
      document.getElementById("eweight").innerHTML = eweight;
      weightVal = false;
  } else {
      eweight = "";
      document.getElementById("eweight").innerHTML = eweight;
      weightVal = true;
  }


  if (height <= 0) {
      eheight = "*Height not valid*";
      document.getElementById("eheight").innerHTML = eheight;
      heightVal = false;
  } else {
      eheight = "";
      document.getElementById("eheight").innerHTML = eheight;
      heightVal = true;
  }

  if (width <= 0) {
      ewidth = "*Width not valid*";
      document.getElementById("ewidth").innerHTML = ewidth;
      widthVal = false;
  } else {
      ewidth = "";
      document.getElementById("ewidth").innerHTML = ewidth;
      widthVal = true;
  }

  if (length <= 0) {
      elength = "*Length not valid*";
      document.getElementById("elength").innerHTML = elength;
      lengthVal = false;
  } else {
      elength = "";
      document.getElementById("elength").innerHTML = elength;
      lengthVal = true;
  }
  if (heightVal == true && widthVal == true && lengthVal == true) {
      furnitureVal = true;
  } else {
      furnitureVal = false;
  }

  if (priceVal == true && productTypeVal == true && skuVal == true && nameVal == true) {
      if (siezeVal == true || weightVal == true || furnitureVal == true) {
          return true;
      } else {
          return false;
      }
  } else {
      return false;
  }
}
