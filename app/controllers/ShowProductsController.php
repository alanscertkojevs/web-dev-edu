<?php

namespace app\controllers;

class ShowProductsController extends Controller
{
    public function act()
    {
        $products = $this->getModel() -> getProducts();
        $content = $this->getView() -> render('app/templates/product.php', array('product'=> $products));
        echo $this -> getView() -> render('app/templates/layout.php', array('title'=> 'Product page', 'content' => $content));
    }
}