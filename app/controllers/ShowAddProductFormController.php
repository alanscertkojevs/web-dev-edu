<?php

namespace app\controllers;

class ShowAddProductFormController extends Controller
{
    public function act()
    {
        echo $this -> getView() -> render('app/templates/addproduct.php', array('title'=> 'Add product page'));
    }
}