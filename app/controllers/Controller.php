<?php

namespace app\controllers;

use app\classes\ProductsModel;
use app\classes\Template;

abstract class Controller
{
    private $model;
    private $view;

    public function __construct()
    {
        $this ->model = ProductsModel::getInstance();
        $this ->view = Template::getInstance();
    }
    protected function getModel()
    {
        return $this->model;
    }
    protected function getView()
    {
        return $this->view;
    }

    abstract function act();

}
