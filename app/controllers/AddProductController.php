<?php

namespace app\controllers;

use \app\classes\furniture;
use \app\classes\book;
use \app\classes\dvd;

class AddProductController extends Controller
{
    public function act()
    {
        $connection = $this->getModel() -> getDBConnection();

        if (isset($_POST['price'])) {
            $sku = mysqli_real_escape_string($connection, $_POST['sku']);
            $name = mysqli_real_escape_string($connection, $_POST['name']);
            $price = mysqli_real_escape_string($connection, $_POST['price']);
            $product = mysqli_real_escape_string($connection, $_POST['product']);
            $size = mysqli_real_escape_string($connection, $_POST['size']);
            $weight = mysqli_real_escape_string($connection, $_POST['weight']);
            $length = mysqli_real_escape_string($connection, $_POST['length']);
            $width = mysqli_real_escape_string($connection, $_POST['width']);
            $height = mysqli_real_escape_string($connection, $_POST['height']);

            if ($product == 'Book') {
                $book = new book(['sku'=>$sku,'name'=>$name,'price'=>$price,'weight'=>$weight]);
                $products = $this->getModel() -> addProduct($book);
                header("Location: index.php");
            }
            if ($product == 'DVD') {
                $dvd = new dvd(['sku'=>$sku,'name'=>$name,'price'=>$price,'size'=>$size]);
                $products = $this->getModel() -> addProduct($dvd);
                header("Location: index.php");
            }
            if ($product == 'Furniture') {
                $furniture = new furniture(['sku'=>$sku,'name'=>$name,'price'=>$price,'length'=>$length,'width'=>$width,'height'=>$height]);
                $products = $this->getModel() -> addProduct($furniture);
                header("Location: index.php");
            }
        }

        $products = $this->getModel() -> getProducts();
        $content = $this->getView() -> render('templates/product.php', array('product'=> $products));
        echo $this -> getView() -> render('templates/layout.php', array('title'=> 'Product page', 'content' => $content));
    }
}