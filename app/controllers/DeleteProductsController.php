<?php

namespace app\controllers;

class DeleteProductsController extends Controller
{
    public function act()
    {
        if (isset($_POST['massDelete'])) {
            if (isset($_POST['delete-checkbox'])) {
                $IDs = $_POST['delete-checkbox'];
                $this->getModel() -> deleteProducts($IDs);
                header("Location: /");
            } else {
                header("Location: /");
            }
        }
        $products = $this->getModel() -> getProducts();
        $content = $this->getView() -> render('templates/product.php', array('product'=> $products));
        echo $this -> getView() -> render('templates/layout.php', array('title'=> 'Product page', 'content' => $content));
    }
}