<?php

namespace app\controllers;

class ShowPageIsNotFoundController extends Controller
{
    public function act()
    {
        echo "This page doesnt exist!";
    }
}
