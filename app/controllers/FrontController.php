<?php

namespace app\controllers;

class FrontController
{
    private $url;
    private $httpMethod;

    protected function getURL()
    {
        return $this->url;
    }
    protected function getHttpMethod()
    {
        return $this->httpMethod;
    }
    private function __construct()
    {
        $this->url = $_SERVER['REQUEST_URI'] ?? '/';
        $this->httpMethod = $_SERVER['REQUEST_METHOD'] ?? 'GET';
    }
    public static function getInstance()
    {
        static $instance;
        if (!isset($instance)) {
            $instance = new self();
        }
        return $instance;
    }
    public function route()
    {
        if ($this->getURL() == "/" && $this->getHttpMethod() === 'GET') {
            $object = new ShowProductsController();
            $object ->act();
        } elseif ($this->getURL() == "/addProduct" && $this->getHttpMethod() === 'GET') {
            $object = new ShowAddProductFormController();
            $object ->act();
        } elseif ($this->getURL() == "/index.php" && $this->getHttpMethod() === 'POST' && isset($_POST['massDelete'])) {
            $object = new DeleteProductsController();
            $object ->act();
        } elseif ($this->getURL() == "/addProduct" && $this->getHttpMethod() === 'POST' && isset($_POST['price'])) {
            $object = new AddProductController();
            $object ->act();
        } else {
            $object = new ShowPageIsNotFoundController();
            $object ->act();
        }
    }
}
