<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="app/styles/main.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="app/jscript/main.js"></script>
    <title><?= $title; ?></title>
</head>
<body>
        <nav class="navbar" role="navigation">
            <h2 class="mt-0"><?= $title; ?></h2>
                <form action= "" onsubmit = "return validate()" method = "post" id="product_form">
            <ul class="nav navbar-nav navbar-right align-bottom">
                <li>
                    <button type="submit" class="btn btn-outline-dark" name="price">Save</button>
                    <a href="/"><button type="button" class="btn btn-outline-dark ">Cancel</button></a>
                </li>
            </ul>
        </nav>
        <hr>
        <div class="content">
        <div class="form-group row">
            <label class="col-md-1 col-form-label">SKU</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="sku" name="sku" placeholder=#sku>
                <p id="esku"></p>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-1 col-form-label">Name</label>
            <div class="col-md-3">
                <input type="text" class="form-control" id="name" name="name" placeholder=#name>
                <p id="ename"></p>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-md-1 col-form-label">Price ($)</label>
            <div class="col-md-3">
              <input type="number" class="form-control" id="price" name="price" placeholder=#price>
              <p id="eprice"></p>
            </div>
        </div>
        <div class="form-group row" id="type">
            <label class="col-md-2 col-form-label">Type switcher</label>
            <div class="col-md-2">
                <select class="form-control" name="product" id="productType" >
                    <option selected hidden>Type switcher</option>
                    <option value="DVD">DVD</option>
                    <option value="Book">Book</option>
                    <option value="Furniture">Furniture</option>
                  </select>
                  <p id="eselect"></p>
            </div>
        </div>
        <div class="container-fluid" id="showDVD">
            <div class="form-group row">
                <label class="col-md-1 col-form-label">Size (MB)</label>
                <div class="col-md-3">
                <input type="number" class="form-control" name="size" id="size" placeholder=#size>
                <p id="esize"></p>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="showFurniture">
            <div class="form-group row">
                <label class="col-md-1 col-form-label">Height (CM)</label>
                <div class="col-md-3">
                <input type="number" class="form-control" name="height" id="height" placeholder=#height >
                <p id="eheight"></p>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-1 col-form-label">Width (CM)</label>
                <div class="col-md-3">
                <input type="number" class="form-control"  name="width" id="width" placeholder=#width >
                <p id="ewidth"></p>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-1 col-form-label">Length (CM)</label>
                <div class="col-md-3">
                <input type="number" class="form-control"  name="length" id="length" placeholder=#length >
                <p id="elength"></p>
                </div>
            </div>
        </div>
        <div class="container-fluid" id="showBook">
            <div class="form-group row">
                <label class="col-md-1 col-form-label">Weight (KG)</label>
                <div class="col-md-3">
                <input type="number" class="form-control" name="weight" id="weight" placeholder=#weight>
                <p id="eweight"></p>
                </div>
            </div>
        </div>
      </form>
    </div>
    <footer id="footer">
        <hr>
        <p class="text-center">Scandiweb Test assignment</p>
    </footer>
</body>
</html>