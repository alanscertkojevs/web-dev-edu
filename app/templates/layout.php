<!DOCTYPE html>
<html>
<head>
<meta charset='utf-8'>
<title><?= $title; ?></title>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<link href="app/styles/main.css" rel="stylesheet">
</head>
<body>
    <nav class="navbar" role="navigation">
        <h2 class="mt-0"><?= $title ?></h2>
          <form action="index.php" method="post">
            <ul class="nav navbar-nav navbar-right align-bottom">
                <li>
                    <a href="addProduct">
                      <button type="button" class="btn btn-outline-dark">ADD</button>
                    </a>
                      <button type="submit" class="btn btn-outline-dark" name="massDelete">MASS DELETE</button>
                </li>
            </ul>
    </nav>
    <div class="content">
    <hr>
      <?= $content ?>
    </div>
          </form>
    <footer id="footer">
        <hr>
        <p class="text-center">Scandiweb Test assignment</p>
    </footer>
</body>
</html>