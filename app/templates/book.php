<div class='book'>
    <span class='sku'><?= $book->getSKU() ?></span>
    <span class='name'><?= $book->getName() ?></span>
    <span class='price'><?= $book->getPrice() ?></span>
    <span class='weight'><?= $book->getWeight() ?></span>
</div>