<div class="row">
    <?php
    foreach ($product as $value)
    {
    ?>
          <div class="col-3">
            <div class="card">
              <div class="card-body">
                <input type="checkbox" name="delete-checkbox[]" class="delete-checkbox" value="<?php echo $value ->getSKU(); ?>" id="flexCheckDefault">
                <p class="text-center"><?= $value ->getSKU(); ?></p>
                <p class="text-center"><?= $value ->getName(); ?></p>
                <p class="text-center"><?= $value ->getPrice(); ?> $</p>
                <p class="text-center"><?php
                if(method_exists($value,'getSize'))
                {
                    echo "Size " , $value -> getSize() . " MB";
                }
                if(method_exists($value,'getWeight'))
                {
                    echo "Weight " . $value ->getWeight() . "KG" ;
                }
                if(method_exists($value,'getLength') && method_exists($value,'getWidth') && method_exists($value,'getHeight'))
                {
                    echo "Dimensions: " . $value ->getLength() . "x" . $value ->getWidth() . "x" . $value ->getHeight() . "x";
                }
                ?></p>
              </div>
            </div>
          </div>
          <?php
        }
    ?>
        </div>    