<?php 
?>
<html>
<head>
    <title>
        <?php
     if (isset($title)):
        echo $title;
    endif;  
        ?>
    </title>
</head>
<body>
    <?php
    if (isset($content)):
        echo $content;
    endif; 
    ?>
    <ul>
    <?php 
    foreach ($vars as $value):
        if (isset($value)):
            echo "<li> $value </li>\n";
        endif;
    endforeach;
    ?>
    </ul>
</body>
</html>
