<?php
namespace app\classes;

class Dvd extends Product
{
    private $size;

    public function getsize()
    {
        return $this->size;
    }
    public function setsize($size)
    {
        $this->size = $size;
    }
    public function __construct($data)
    {
        parent::__construct($data);
        $this->setsize($data['size']);
    }
    public function __toString()
    {
        return (new Template)->render('add/templates/dvd.php', array('dvd' => $this));
    }
}
?>