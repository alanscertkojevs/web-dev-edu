<?php
namespace app\classes;

    class Template
    {
        private function __construct()
        {

        }
        public static function getInstance()
        {
            static $instance;
            if(!isset($instance))
            {
                $instance = new self;
            }
            return $instance;
        }
        public function render($path,$vars = [])
        {
            if (is_array($vars) && !empty($vars)) {
                extract($vars);
            }
            ob_start();
            include $path;
            return ob_get_clean();
        }
    }
?>