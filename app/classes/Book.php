<?php
namespace app\classes;

class Book extends Product
{
    private $weight;

    public function getWeight()
    {
        return $this->weight;
    }
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }
    public function __construct($data)
    {
        parent::__construct($data);
        $this->setWeight($data['weight']);
    }
    public function __toString()
    {
        return (new Template)->render('add/templates/book.php', array('book' => $this));
    }
}
