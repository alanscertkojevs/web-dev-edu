<?php
namespace app\classes;

//use app\classes\product;
use app\classes\Book;
use app\classes\Dvd;
use app\classes\Furniture;

class ProductsModel
{
    private $host = "localhost";
    private $user = "root";
    private $password = "";
    private $database = "web";
    private $dbConnection;

    public function __construct()
    {
        $this->dbConnection = mysqli_connect($this-> host ,$this-> user ,$this-> password ,$this-> database);
        $this->dbConnection->set_charset('utf8mb4');
    }
    public function getDBConnection()
    {
        return $this->dbConnection;
    }
    public function addProduct($product)
    {
        $parameters = ['sku', 'name', 'price', 'weight', 'size', 'length', 'width', 'height'];
        $queryInput = [];
        $reflect = new \ReflectionClass($product);
        $queryInput['type'] = $reflect->getShortName();

        foreach ($parameters as $parameter)
        {
            $queryInput[$parameter] = method_exists($product, $method = "get{$parameter}") ? $product->$method() : 'null';
        }
        $query = " INSERT INTO product (sku,name,price,type,weight,size,length, width, height)
        VALUES (
            '{$queryInput["sku"]}' ,
            '{$queryInput["name"]}',
            {$queryInput["price"]},
            '{$queryInput["type"]}',
            {$queryInput["weight"]},
            {$queryInput["size"]},
            {$queryInput["length"]},
            {$queryInput["width"]},
            {$queryInput["height"]}
            );";
            $this->getDBConnection()->query($query);
    }
    public function getProducts()
    {
        $query = 'SELECT * FROM `product` ORDER BY `sku` ASC;';
        $result =  $this->getDBConnection()->query($query);
        $products = [];
        while ($row = $result->fetch_assoc())
        {
            $productType = "app\classes\\{$row['type']}";
            $products[] = new $productType($row);
        }
        return $products;
    }
    public function deleteProducts($skus)
    {
        $sku = implode("','", $skus);
        $query = "DELETE FROM `product` WHERE `sku` in ('".$sku."');";
        $this->getDBConnection()-> query($query);
    }
    public static function getInstance()
    {
        static $instance;
        if(!isset($instance))
        {
            $instance = new self;
        }
        return $instance;
    }
}