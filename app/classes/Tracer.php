<?php
namespace app\classes;

 class Tracer
 {
	 public static function trace($val)
	 {
		echo '<pre>';
		var_dump($val);
		echo '</pre>';
	 }
	 public static function traceAndExit($val)
	 {
		self::trace($val);
		exit();	
	 }
 }
?>