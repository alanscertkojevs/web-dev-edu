<?php
namespace app\classes;

class Furniture extends Product
{
    private $length;
    private $width;
    private $height;

    public function getLength()
    {
        return $this->length;
    }
    public function getWidth()
    {
        return $this->width;
    }
    public function getHeight()
    {
        return $this->height;
    }
    public function setLength($length)
    {
        $this->length = $length;
    }
     public function setWidth($width)
    {
        $this->width = $width;
    }
     public function setHeight($height)
    {
        $this->height = $height;
    }
    public function __construct($data)
    {
        parent::__construct($data);
        $this->setLength($data['length']);
        $this->setWidth($data['width']);
        $this->setHeight($data['height']);
    }
    public function __toString()
    {
        return (new Template)->render('add/templates/furniture.php', array('furniture' => $this));
    }
}
