<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'fccf5ad371fdb77b8de3f4b202d235e42e125bc6',
        'name' => 'bob/web',
        'dev' => true,
    ),
    'versions' => array(
        'bob/web' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'fccf5ad371fdb77b8de3f4b202d235e42e125bc6',
            'dev_requirement' => false,
        ),
    ),
);
