<?php

// autoload_real.php @generated by Composer

class ComposerAutoloaderInitbc6d1660a943ce7cf8b93d5f97d6b733
{
    private static $loader;

    public static function loadClassLoader($class)
    {
        if ('Composer\Autoload\ClassLoader' === $class) {
            require __DIR__ . '/ClassLoader.php';
        }
    }

    /**
     * @return \Composer\Autoload\ClassLoader
     */
    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }

        spl_autoload_register(array('ComposerAutoloaderInitbc6d1660a943ce7cf8b93d5f97d6b733', 'loadClassLoader'), true, true);
        self::$loader = $loader = new \Composer\Autoload\ClassLoader(\dirname(__DIR__));
        spl_autoload_unregister(array('ComposerAutoloaderInitbc6d1660a943ce7cf8b93d5f97d6b733', 'loadClassLoader'));

        require __DIR__ . '/autoload_static.php';
        call_user_func(\Composer\Autoload\ComposerStaticInitbc6d1660a943ce7cf8b93d5f97d6b733::getInitializer($loader));

        $loader->register(true);

        return $loader;
    }
}
